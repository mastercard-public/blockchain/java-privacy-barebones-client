# Java Barebones Privacy Client

### What you will need
 * The protoc compiler on your workstation (Google's [protocol buffer compiler](https://github.com/google/protobuf/releases))
 * Mastercard Developers credential
  * **If you dont already have a P12 / Consumerkey for the Blockchain API you can obtain one from [Mastercard Developers](https://developers.mastercard.com)** 
 * Blockchain App Id 
  * **If you dont already have this you obtain one from [Mastercard Developers](https://developers.mastercard.com) by creating a Blockchain project**   

### Getting Started
A sample use-case of a 'B2B Transaction' application for the Mastercard Blockchain API. It showcases a 'Sender' node notifying a 'Moderator' node of an agreed transaction between two parties, with the Moderator then verifying and subsequently retrieving the written transaction from the blockchain for settlement.

###### Usage
Firstly run `mvn package` from the project directory root, and then `java -jar target/java-blockchain-barebones-client-0.0.1-SNAPSHOT-jar-with-dependencies.jar -kp <path to p12> -ck <your consumer key` to actually start the application.

When the application has started, you are presented with the following menu options:

```shell
1. Mimic Moderator Node (begin listening as a moderator node)
2. Mimic Send Node (initiate a transaction to a moderator node as a sender node)
3. Update Node (update your blockchain node to the message definition of this project, required if starting the application for the first time)
4. Create transaction (manually create a transaction)
5. Retrieve transaction (manually retrieve a transaction)
```

Input '1' to begin listening for a transaction HTTP request as a 'moderator' node (note that the `localhost:8080` socket must be free).

You will then need to start a second instance of the application using the instructions above again. It can use the same consumer key and P12 file, although it does not need to.

Once the second application has started, input '2' to initiate a transaction to the 'moderator' node as a 'sender' node. You will see console output from both nodes as the transaction goes through multiple stages:

* The Sender sends a HTTP request to the Moderator with a hypothetical transaction agreed between two parties.
* The Moderator verifies this transaction (always successful for this demo app), and then returns a reference field unique to this transaction to the Sender (hardcoded in this case).
* The Moderator then generates a hash from all the transaction information it has and the reference field it has given, and immediately begins polling the Blockchain looking for this hash in anticipation of the Sender node writing it there.
* The Sender node writes its transaction with the reference field from the Moderator's response to the Blockchain.
* The Moderator eventually finds the entry written by the Sender in the Blockchain, and then settles this transaction via a Mastercard API (stubbed out in this app).