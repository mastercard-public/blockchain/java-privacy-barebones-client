package com.mastercard.blockchain;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.protobuf.InvalidProtocolBufferException;
import com.mastercard.api.blockchain.App;
import com.mastercard.api.blockchain.Block;
import com.mastercard.api.blockchain.TransactionEntry;
import com.mastercard.api.core.ApiConfig;
import com.mastercard.api.core.exception.ApiException;
import com.mastercard.api.core.model.RequestMap;
import com.mastercard.api.core.model.ResourceList;
import com.mastercard.api.core.security.oauth.OAuthAuthentication;
import com.sun.net.httpserver.HttpServer;

/**
 * Copyright (c) 2017 Mastercard. All Rights Reserved.
 */
public class B2BApplication {

	private String ENCODING = "base64";
	private String APP_ID = getAppIdFromProtoBuffer();

	private String lastHash = "";
	private String lastSlot = "";

	private MessageDigest hashDigest = getMessageDigest("SHA-256");

	public static void main(String... args) throws Exception {
		CommandLineParser parser = new DefaultParser();
		Options options = createOptions();
		CommandLine cmd = parser.parse(options, args);
		new B2BApplication().start(cmd, options);
	}

	private static MessageDigest getMessageDigest(String algorithm) {
		try {
			return MessageDigest.getInstance(algorithm);
		} catch (NoSuchAlgorithmException e) {
			System.err.println("Error generating Message Digest: " + e.getMessage());
			return null;
		}
	}

	private void start(CommandLine cmd, Options options) throws FileNotFoundException {
		System.out.println(readResourceToString("/help.txt"));
		System.out.println();
		initApi(cmd);
		updateNode(true);
		menu(cmd, options);
	}

	private void menu(CommandLine cmd, Options options) {
		final String quit = "0";
		String option = "";
		while (!option.equals(quit)) {
			printHeading("MENU");
			System.out.println("1. Mimic Moderator Node");
			System.out.println("2. Mimic Send Node");
			System.out.println("3. Update Node");
			System.out.println("4. Create transaction");
			System.out.println("5. Retrieve transaction");
			System.out.println(quit + ". Quit");
			option = captureInput("Option", quit);
			if (option.equals("1")) {
				printHeading("MODERATOR NODE");
				beginModeratorNode();
			} else if (option.equals("2")) {
				printHeading("SENDER NODE");
				beginSenderNode();
			} else if (option.equals("3")) {
				printHeading("UPDATE NODE");
				updateNode(false);
			} else if (option.equals("4")) {
				printHeading("CREATE TRANSACTION");
				createTransactionEntry();
			} else if (option.equals("5")) {
				printHeading("RETRIEVE TRANSACTION");
				retrieveTransaction();
			} else if (option.equals(quit)) {
				System.out.println("Goodbye");
			} else {
				System.out.println("Unrecognised option");
			}
		}
	}

	private Block pollBlockchain(String hashToFind, String slotToStart, long millisToPoll) {
		long desiredEndTime = System.currentTimeMillis() + millisToPoll;

		String lastPolledBlock = (slotToStart == null || slotToStart.equals("") ? null : slotToStart);
		Optional<Block> matchingBlock = Optional.empty();

		do {
			RequestMap map = new RequestMap();

			if (lastPolledBlock != null) {
				map.set("from", lastPolledBlock);
			}

			try {
				System.out.println("Polling from "
						+ (lastPolledBlock == null ? "last confirmed slot number" : "slot " + lastPolledBlock) + "...");
				ResourceList<Block> blocks = Block.list(map);
				List<Block> blockList = blocks.getList();
				int noOfBlocks = blockList.size();

				if (noOfBlocks > 0) {
					matchingBlock = (blockList.stream().filter(block -> {
						JSONArray partitions = (JSONArray) block.get("partitions");

						return partitions.stream().anyMatch(partition -> {
							JSONArray entries = (JSONArray) ((JSONObject) partition).get("entries");
							if (entries.size() > 0) {
								return entries.stream().anyMatch(entry -> entry.toString().equals(hashToFind));
							} else {
								return false;
							}
						});
					})).findFirst();

					lastPolledBlock = blockList.get(noOfBlocks - 1).get("slot").toString();
				}

			} catch (ApiException e) {
				System.err.println("API Exception: " + e.getMessage());
				break;
			}
		} while (System.currentTimeMillis() < desiredEndTime && !matchingBlock.isPresent());

		return matchingBlock.isPresent() ? matchingBlock.get() : null;
	}

	private String generateHash(byte[] bytes) {
		return Hex.encodeHexString(hashDigest.digest(hashDigest.digest(bytes)));
	}

	private void beginModeratorNode() {
		String port = captureInput("Listening port", "8080");
		HttpServer server = null;
		try {
			server = HttpServer.create(new InetSocketAddress(Integer.parseInt(port)), 0);
		} catch (IOException e) {
			e.printStackTrace();
		}
		server.createContext("/payment", httpExchange -> {
			System.out.println("Payment request received...");
			String requestBody;
			String hashToFind = null;
			try (BufferedReader buffer = new BufferedReader(new InputStreamReader(httpExchange.getRequestBody()))) {
				requestBody = buffer.lines().collect(Collectors.joining(System.lineSeparator()));
			}
			try {
				JSONObject jsonBody = (JSONObject) new JSONParser().parse(requestBody);
				JSONObject responseBody = new JSONObject();

				if (checkTransactionValidity(jsonBody)) {
					String ref = "ae21bc34";
					System.out.println("Payment validated, returning moderator reference of " + ref);
					byte[] transactionBuffer = createTransactionBuffer(jsonBody.get("from").toString(),
							jsonBody.get("to").toString(), Integer.parseInt(jsonBody.get("amount").toString()), ref);
					hashToFind = generateHash(transactionBuffer);

					responseBody.put("moderatorRef", ref);
					responseBody.put("valid", true);
				} else {
					responseBody.put("moderatorRef", "");
					responseBody.put("valid", false);
				}

				httpExchange.sendResponseHeaders(200, responseBody.toJSONString().length());
				OutputStream os = httpExchange.getResponseBody();
				os.write(responseBody.toJSONString().getBytes());
				os.close();

				if (hashToFind != null) {
					System.out.println(
							"Beginning polling of blockchain for prospective transaction with hash " + hashToFind);
					Block block = pollBlockchain(hashToFind, null, 30000);
					if (block != null) {
						System.out.println("Transaction found at slot " + block.get("slot")
								+ ". Sending payment to Mastercard settlement API.");
					} else {
						System.out.println("Entry not found within time limit, exiting poll.");
					}
				}
			} catch (ParseException e) {
				System.out.println("Error parsing JSON body: " + e.toString());
				httpExchange.sendResponseHeaders(400, -1);
			}
		});
		server.start();
		captureInput("(press return at any time to stop server)", null);
		server.stop(0);
	}

	private boolean checkTransactionValidity(JSONObject jsonBody) {
		return true;
	}

	private void beginSenderNode() {
		String from = captureInput("From account", "iban:15637");
		String to = captureInput("To account", "iban:243411");
		int amount = Integer.parseInt(captureInput("Transaction amount", "231"));
		int targetPort = Integer.parseInt(captureInput("Target port", "8080"));

		try {
			System.out.println("Making request to moderator node...");
			URL url = new URL("http://127.0.0.1:" + targetPort + "/payment");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");

			JSONObject prospectivePayment = new JSONObject();
			prospectivePayment.put("from", from);
			prospectivePayment.put("to", to);
			prospectivePayment.put("amount", amount);
			String jsonBody = prospectivePayment.toJSONString();

			connection.setRequestProperty("Content-Length", Integer.toString(jsonBody.length()));

			connection.setUseCaches(false);
			connection.setDoOutput(true);

			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(jsonBody);
			wr.close();

			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
			String line;
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();

			JSONObject responseBody = (JSONObject) new JSONParser().parse(response.toString());
			boolean isValid = Boolean.parseBoolean(responseBody.get("valid").toString());
			String moderatorRef = responseBody.get("moderatorRef").toString();
			if (isValid) {

				System.out
						.println("Response received: payment is valid and has settlement reference of " + moderatorRef);

				TransactionEntry newEntry = createTransactionEntry(from, to, amount, moderatorRef);
				if (newEntry != null) {
					System.out.println("Transaction written to chain at slot " + newEntry.get("slot").toString());
					printTransaction(newEntry);
				} else {
					System.err.println("Error occured writing to transaction to chain.");
				}
			} else {
				System.err.println("Payment is invalid and rejected by moderator node");
			}
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}

		captureInput("(press return to continue)", null);
	}

	private void updateNode(boolean silently) {
		String protoPath = captureInput("Protocol Definition Path", "/transaction.proto");

		try {
			RequestMap map = new RequestMap();
			map.set("id", APP_ID);
			map.set("name", APP_ID);
			map.set("description", "");
			map.set("version", 0);
			map.set("definition.format", "proto3");
			map.set("definition.encoding", "base64");
			map.set("definition.messages",
					Base64.getEncoder().encodeToString(readResourceToString(protoPath).getBytes()));
			new App(map).update();
			System.out.println("Node updated");
			App app = App.read(APP_ID);
			if (!silently) {
				JSONObject definition = (JSONObject) app.get("definition");
				System.out.println("New Format: " + definition.get("format"));
				System.out.println("New Encoding: " + definition.get("encoding"));
				System.out.println("New Messages: " + definition.get("messages"));
			}
		} catch (ApiException e) {
			System.err.println("API Exception " + e.getMessage());
		}
		if (!silently) {
			captureInput("(press return to continue)", null);
		}
	}

	private TransactionEntry retrieveEntry(String hash) {
		try {
			RequestMap request = new RequestMap();
			request.put("hash", hash);
			TransactionEntry response = TransactionEntry.read("", request);
			return response;
		} catch (ApiException e) {
			System.err.println("API Exception: " + e.getMessage());
			return null;
		}
	}

	private void createTransactionEntry() {
		String from = captureInput("from", "iban:14214212");
		String to = captureInput("to", "iban:42356864");
		int amount = Integer.parseInt(captureInput("amount", "21551"));
		String moderatorRef = captureInput("moderator reference", "aeb31a");

		TransactionEntry newTransaction = createTransactionEntry(from, to, amount, moderatorRef);
		printTransaction(newTransaction);

		captureInput("(press return to continue)", null);
	}

	private TransactionEntry createTransactionEntry(String from, String to, int amount, String moderatorRef) {
		TransactionEntry response = null;
		try {
			byte[] buffer = createTransactionBuffer(from, to, amount, moderatorRef);
			String encoded = encode(buffer, ENCODING);
			RequestMap request = new RequestMap();
			request.put("app", APP_ID);
			request.put("encoding", ENCODING);
			request.put("value", encode(buffer, ENCODING));
			response = TransactionEntry.create(request);
			lastHash = response.get("hash").toString();
			lastSlot = response.get("slot").toString();
		} catch (ApiException e) {
			System.err.println("API Error occured: " + e.getMessage());
		}

		return response;
	}

	byte[] createTransactionBuffer(String from, String to, int amount, String moderatorRef) {
		TransactionProtocolBuffer.Transaction.Builder builder = TransactionProtocolBuffer.Transaction.newBuilder();
		TransactionProtocolBuffer.Transaction protocolBuffer = builder.setFrom(from).setTo(from).setAmount(amount)
				.setModeratorRef(moderatorRef).build();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			protocolBuffer.writeTo(baos);
		} catch (IOException e) {
			System.out.println("IOException writing buffer " + e.getMessage());
		}
		return baos.toByteArray();
	}

	private void retrieveTransaction() {
		String hash = captureInput("hash", lastHash);
		TransactionEntry response = retrieveEntry(hash);
		printTransaction(response);
		captureInput("(press return to continue)", null);
	}

	private void printTransaction(TransactionEntry response) {
		System.out.println("Hash: " + response.get("hash").toString());
		System.out.println("Slot: " + response.get("slot").toString());
		System.out.println("Status: " + response.get("status").toString());
		if (response.containsKey("value")) {
			String hexEncoded = response.get("value").toString();
			System.out.println("Value: " + hexEncoded);
			try {
				TransactionProtocolBuffer.Transaction message = TransactionProtocolBuffer.Transaction
						.parseFrom(decode(hexEncoded, "hex"));
				System.out.println("Decoded Value: to -> " + message.getTo() + " from -> " + message.getFrom()
						+ " amount -> " + message.getAmount() + " moderatorRef -> " + message.getModeratorRef());
			} catch (DecoderException | InvalidProtocolBufferException e) {
				System.err.println("Unable to decode: " + hexEncoded);
			}
		}
	}

	private void initApi(CommandLine cmd) throws FileNotFoundException {
		String keystorePath = captureInputFile("Keystore", cmd.getOptionValue("keystorePath", ""));
		String storePass = captureInput("Keystore Password", cmd.getOptionValue("storePass", "keystorepassword"));
		String consumerKey = captureInput("Consumer Key", cmd.getOptionValue("consumerKey", ""));
		String keyAlias = captureInput("Key Alias", cmd.getOptionValue("keyAlias", "keyalias"));

		ApiConfig.setAuthentication(
				new OAuthAuthentication(consumerKey, new FileInputStream(keystorePath), keyAlias, storePass));
		ApiConfig.setDebug(false);
		ApiConfig.setSandbox(true);
	}

	private String captureInputFile(String question, String defaultAnswer) {
		boolean noFile = true;
		String keystorePath = null;
		while (noFile) {
			keystorePath = captureInput(question, defaultAnswer);
			keystorePath = keystorePath.replaceFirst("^~/", System.getProperty("user.home") + "/");
			if (Files.notExists(Paths.get(keystorePath))) {
				System.out.println("File Not Found");
			} else {
				noFile = false;
			}
		}
		return keystorePath;
	}

	private String captureInput(String question, String defaultAnswer) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		if (defaultAnswer == null) {
			System.out.print(question + ": ");
		} else {
			System.out.print(question + " [" + defaultAnswer + "]: ");
		}
		String s;
		try {
			s = br.readLine();
			if (s == null || "".equals(s)) {
				s = defaultAnswer;
			}
		} catch (IOException e) {
			s = defaultAnswer;
		}
		return s;
	}

	private void printHeading(String heading) {
		System.out.println("============ " + heading + " ============");
	}

	private static String readResourceToString(String path) {
		return new BufferedReader(new InputStreamReader(B2BApplication.class.getResourceAsStream(path))).lines()
				.collect(Collectors.joining("\n"));
	}

	private String encode(byte[] bytes, String encoding) {
		if (encoding.equals("hex")) {
			return Hex.encodeHexString(bytes);
		} else {
			return Base64.getEncoder().encodeToString(bytes);
		}
	}

	private byte[] decode(String encoded, String encoding) throws DecoderException {
		if (encoding.equals("hex")) {
			return Hex.decodeHex(encoded.toCharArray());
		} else {
			return Base64.getDecoder().decode(encoded);
		}
	}

	public static String getAppIdFromProtoBuffer() {
		String protoBuf = readResourceToString("/transaction.proto");
		Pattern pattern = Pattern.compile("package\\s(.[A-Za-z0-9]+);", Pattern.MULTILINE);
		Matcher m = pattern.matcher(protoBuf);
		if (m.find()) {
			return m.group(1);
		}
		return "";
	}

	private static Options createOptions() {
		Options options = new Options();

		options.addOption("ck", "consumerKey", true, "consumer key (mastercard developers)");
		options.addOption("kp", "keystorePath", true, "the path to your keystore (mastercard developers)");
		options.addOption("ka", "keyAlias", true, "key alias (mastercard developers)");
		options.addOption("sp", "storePass", true, "keystore password (mastercard developers)");
		options.addOption("v", "verbosity", false, "log mastercard developers sdk to console");

		return options;
	}

}
